import React, { Component } from 'react';
import {BrowserRouter, Route, NavLink} from 'react-router-dom';
import './App.css';

/******************************
 * Ваша реализация компонента
 *****************************/
const Menu = props => {
  return (
    <div className="tabs">
      <nav className="tabs__items">
        <NavLink exact activeClassName="tabs__item-active" className="tabs__item" to="/">Рефераты</NavLink>
        <NavLink activeClassName="tabs__item-active" className="tabs__item" to="/creator">Криэйтор</NavLink>
        <NavLink activeClassName="tabs__item-active" className="tabs__item" to="/fortune">Гадалка</NavLink>
      </nav>
    </div>    
  );

}

class App extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <div className="tabs__content">
          <Menu/>
          <div>
            <Route path="/" exact component={Creator} />
            <Route path="/creator" component={Essay} />
            <Route path="/fortune" component={Fortune} />
          </div>
        </div>
      </BrowserRouter>
    );
  }
}

/******************************
 * Не вносить изменния ниже
 ******************************/
const Creator = props => {
  return (
    <div>
      Уникальные, ёмкие слоганы, сочиненные дежурным роботом-криэйтором – 
      незаменимый помощник при творческом кризисе. 
      Просто введите название компании или продукта, и робот всё сделает за вас. 
    </div>
  )
}

const Essay = props => {
  return (
    <div>
      Нажав на кнопку «Написать реферат», вы лично 
      создаете уникальный текст, причем именно от вашего 
      нажатия на кнопку зависит, какой именно текст 
      получится. Теперь никто не сможет обвинить вас в плагиате, 
      ибо каждый текст неповторим.
    </div>
  )
}

const Fortune = props => {
  return (
    <div>
      Загадайте вопрос. Введите ключевое слово этого вопроса 
      (имя, событие и пр.) в строку, нажмите кнопку «узнать» и 
      получите лаконичный ответ. Расшифровать послания каждому 
      придется самостоятельно, так как для разных людей одни 
      и те же слова имеют разное значение.
    </div>
  )
}

export default App;
