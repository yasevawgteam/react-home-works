import React, { Component } from 'react';
import Label from './Label';
import ChartItem from './ChartItem';

function compareNumbers(a, b) {
    return a - b;
}

function ChartSerie({className, ...state}) {
    return (
        state.data.map((serie, serieIndex) => {
            let sortedSerie = serie.slice(0),
                sum;

            sum = serie.reduce((carry, current) => carry + current, 0);
            sortedSerie.sort(compareNumbers);

            return (
                <div className={className}
                     key={ serieIndex }
                     style={{ height: 250 }}
                >
                    <Label {...state} serieIndex={serieIndex}/>

                    <ChartItem className={className} sum={sum} serie={serie} {...state}/>
                </div>
            );
        })
    );
}

export default ChartSerie;