import React, { Component } from 'react';

const Label = props => <label>{ props.labels[props.serieIndex] }</label>;

export default Label;