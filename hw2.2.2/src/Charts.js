import React, { Component } from 'react';

function Charts(props) {
    //console.log(props);
    return (
        <div className={props.className}>
            {props.children}
        </div>
    );
}

export default Charts;