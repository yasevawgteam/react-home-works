import React, { Component } from 'react';

function ChartItem(props) {
    let className = props.className.split(" ")[1];

    return (
        props.serie.map((item, itemIndex) => {
            let color = props.colors[itemIndex], style,
                size = item / props.sum * 100;

            style = {
                backgroundColor: color,
                opacity: 1,
                zIndex: item,
                height: size + '%'
            };

            return (
                <div
                    className={`Charts--item ${className}`}
                    style={ style }
                    key={ itemIndex }
                >
                    <b style={{ color: color }}>{ item }</b>
                </div>
            );
        })
    );
}

export default ChartItem;