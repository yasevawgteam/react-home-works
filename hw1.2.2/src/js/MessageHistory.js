import React from 'react';
import Message from './Message';
import Response from './Response';
import Typing from './Typing';

function MessageHistory ({list = []}) {
    if (list.length === 0) return null;
    return (
        <ul>
            {
                list.map(mess => {
                    if (mess.type === 'message') {
                        return <Message from={mess.from} message={mess} key={mess.id}/>
                    } else if (mess.type === 'response') {
                        return <Response from={mess.from} message={mess} key={mess.id}/>
                    } else {
                        return <Typing from={mess.from} message={mess} key={mess.id}/>
                    }
                }
            )}
        </ul>
    );
}

export default MessageHistory;