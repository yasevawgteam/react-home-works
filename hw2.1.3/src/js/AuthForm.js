import React from 'react';

function AuthForm (props) {
    let user = {};

    const checkName = (event) => {
        user.name = event.currentTarget.value;
    };
    const checkEmail = (event) => {
        let reg = /[А-Яа-я\,\;\:\'\"\{\}\[\]\=\!\?\%\^\&\*\(\)\#\$\|\+]/;
        event.currentTarget.value = event.currentTarget.value.replace(reg, '');
        user.email = event.currentTarget.value;
    };
    const checkPassword = (event) => {
        let reg = /[А-Яа-я\,\;\:\'\"\{\}\[\]\=\!\?\%\^\&\*\(\)\#\$\|\+\@\-]/;
        event.currentTarget.value = event.currentTarget.value.replace(reg, '');
        user.password = event.currentTarget.value;
    };

    function onAuth (event) {
        event.preventDefault();
        props.onAuth(user);
        //console.log(user);
    }

    return (
        <form className="ModalForm" action="/404/auth/" method="POST">
            <div className="Input">
                <input required type="text" placeholder="Имя" onChange={checkName} id="name"/>
                    <label></label>
            </div>
            <div className="Input">
                <input type="email" placeholder="Электронная почта" onChange={checkEmail}/>
                    <label></label>
            </div>
            <div className="Input">
                <input required type="password" placeholder="Пароль" onChange={checkPassword}/>
                    <label></label>
            </div>
            <button type="submit" onClick={onAuth}>
                <span>Войти</span>
                <i className="fa fa-fw fa-chevron-right"></i>
            </button>
        </form>
    );
}

export default AuthForm;