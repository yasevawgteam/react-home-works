import React from 'react';

const ModalResult = props => {
    if (props.isHidden) {
        return null;
    }
    return (
        <div className="Modal ModalResult">
            {props.children}
        </div>
    );
};

export default ModalResult;