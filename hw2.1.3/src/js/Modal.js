import React from 'react';

const Modal = props => {
    if (props.isHidden) {
        return null;
    }
    return (
        <div className="Modal">
            {props.children}
        </div>
    );
};

export default Modal;