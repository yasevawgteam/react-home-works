import React from 'react';
import ReactDOM from 'react-dom';
import './css/loader.css';
import './css/main.css';
import Listing from './js/Listing';
import registerServiceWorker from './registerServiceWorker';

function getData () {
    try {
        return fetch('https://neto-api.herokuapp.com/etsy')
            .then(response => response.json())
            .catch(err => console.error(err));
    } catch (e) {
        console.error(e);
    }
}

ReactDOM.render(<Listing />, document.getElementById('root'));

getData().then(data => ReactDOM.render(<Listing items={data} />, document.getElementById('root')));
registerServiceWorker();
