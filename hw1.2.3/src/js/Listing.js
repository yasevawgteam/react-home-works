import React from 'react';

function printCurrency(code, count) {
    let newLabel = code.toUpperCase();
    if (newLabel === "USD") {
        return `$ ${count}`;
    } else if (newLabel === "EUR") {
        return `€ ${count}`;
    } else {
        return `${count} ${code}`;
    }
}

function printQuantity (quantity) {
    if (quantity <= 10) {
        return 'item-quantity level-low';
    } else if (quantity <= 20 && quantity > 10) {
        return 'item-quantity level-medium';
    } else {
        return 'item-quantity level-high';
    }
}

function Listing({items = []}) {
    if (items.length === 0) {
        return (
            <div className="holder">
                <div className="preloader">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        );
    }
    return (
        <div className="item-list">{
            items.map(item => {
                return (
                    <div key={item.listing_id} className="item">
                        <div className="item-image">
                            <a href={item.url}>
                                <img src={item.MainImage.url_fullxfull != undefined ? item.MainImage.url_fullxfull : ''} />
                            </a>
                        </div>
                        <div className="item-details">
                            <p className="item-title">{item.title.substr(0,49)+'...'}</p>
                            <p className="item-price">{ printCurrency(item.currency_code,item.price) }</p>
                            <p className={ printQuantity(item.quantity)}>{item.quantity } pcs</p>
                        </div>
                    </div>
                )
            })
        }
        </div>
    );
}

export default Listing;