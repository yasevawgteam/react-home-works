import React from 'react';

function FeedbackForm (props) {
    let form = {},
        salutationArray = [
            {id : 'salut1', name : 'мисис', shortName : 'mrs'},
            {id : 'salut2', name : 'мис', shortName : 'ms'},
            {id : 'salut3', name : 'мистер', shortName : 'mr'}
        ],
        subjectArray = [
            {id : 'subject1', option : 'У меня проблема'},
            {id : 'subject2', option : 'У меня важный вопрос1'},
            {id : 'subject3', option : 'У меня важный вопрос2'},
            {id : 'subject4', option : 'У меня важный вопрос3'}
        ],
        snacksArray = [
            {id : 'snack1', value : 'пицца', hiddenName : 'pizza', name : 'Пиццу'},
            {id : 'snack2', value : 'пирог', hiddenName : 'cake', name : 'Пирог'}
        ];

    form.snacks = [];

    function saveForm (e) {
        let currVal = e.currentTarget.value, currName = e.currentTarget.name;

        if (e.currentTarget.name === 'snacks') {
            let indexElement = form.snacks.indexOf(currVal);
            indexElement === -1 ? form.snacks.push(currVal) : form.snacks.splice(indexElement, 1);
            console.log(form);
        } else {
            form[currName] = currVal;
            console.log(form);
        }
    }

    function onSubmit () {
        props.onSubmit(JSON.stringify(form));
    }

    const salutation = () => {
        let exitData = [];

        salutationArray.forEach(elem => {
            if (elem.name.toLowerCase() === props.data.salutation.toLowerCase()) {
                exitData.push(
                    <input
                        key={elem.id}
                        defaultChecked
                        onChange={saveForm}
                        className="contact-form__input contact-form__input--radio"
                        id={'salutation-' + elem.shortName}
                        name="salutation"
                        type="radio"
                        value={elem.name}/>
                );
                exitData.push(<label key={'label'+elem.id} className="contact-form__label contact-form__label--radio" htmlFor={"salutation-"+elem.shortName}>{elem.name}</label>);
            } else {
                exitData.push(
                    <input
                        key={elem.id}
                        onChange={saveForm}
                        className="contact-form__input contact-form__input--radio"
                        id={'salutation-' + elem.shortName}
                        name="salutation"
                        type="radio"
                        value={elem.name}/>
                );
                exitData.push(<label key={'label'+elem.id} className="contact-form__label contact-form__label--radio" htmlFor={"salutation-"+elem.shortName}>{elem.name}</label>);
            }
        });

        return (
            <div className="contact-form__input-group">{exitData}</div>
        );
    };

    const name = (
        <div className="contact-form__input-group">
            <label className="contact-form__label" htmlFor="email">Адрес электронной почты</label>
            <input
                defaultValue={props.data.email}
                onChange={saveForm}
                className="contact-form__input contact-form__input--email"
                id="email"
                name="email"
                type="email"/>
        </div>
    );

    const email = (
        <div className="contact-form__input-group">
            <label className="contact-form__label" htmlFor="email">Адрес электронной почты</label>
            <input
                defaultValue={props.data.email}
                onChange={saveForm}
                className="contact-form__input contact-form__input--email"
                id="email"
                name="email"
                type="email"/>
        </div>
    );

    const subject = () => {
        //let indexSubject = subjectArray.forEach(item => item.option.indexOf(props.data.subject));
        //console.log(indexSubject);
        return (
            <div className="contact-form__input-group">
                <label className="contact-form__label" htmlFor="subject">Чем мы можем помочь?</label>
                <select
                    defaultValue={3}
                    onChange={saveForm}
                    className="contact-form__input contact-form__input--select"
                    id="subject"
                    name="subject">
                    { subjectArray.map(elem => <option key={elem.id}>{elem.option}</option>) }
                </select>
            </div>
        );
    };

    const message = (
        <div className="contact-form__input-group">
            <label className="contact-form__label" htmlFor="message">Ваше сообщение</label>
            <textarea
                onChange={saveForm}
                className="contact-form__input contact-form__input--textarea"
                id="message"
                name="message"
                rows="6"
                cols="65">
                </textarea>
        </div>
    );

    const snacks = () => {
        let outputArray = [];
        snacksArray.forEach(item => {
            outputArray.push(
                <input
                    key={item.id}
                    onChange={saveForm}
                    className="contact-form__input contact-form__input--checkbox"
                    id="snacks-pizza"
                    name="snacks"
                    type="checkbox"
                    value={item.value}/>
            );
            outputArray.push(
                <label key={'label'+item.id} className="contact-form__label contact-form__label--checkbox"
                    htmlFor="snacks-pizza">{item.name}</label>
            );
        });
        return(
            <div className="contact-form__input-group">
                <p className="contact-form__label--checkbox-group">Хочу получить:</p>
                {outputArray}
            </div>
        );
    };

    const button = (
        <button onSubmit={onSubmit} className="contact-form__button" type="submit">Отправить сообщение!</button>
    );

    return (
        <form className="content__form contact-form">
            <div className="testing">
                <p>Чем мы можем помочь?</p>
            </div>

            {salutation()}
            {name}
            {email}
            {subject()}
            {message}
            {snacks()}
            {button}

            <output id="result"/>
        </form>
    );
}

export default FeedbackForm;