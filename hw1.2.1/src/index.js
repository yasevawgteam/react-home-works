import React from 'react';
import ReactDOM from 'react-dom';
import './css/main.css';
import Films from './js/Films';
//import registerServiceWorker from './registerServiceWorker';

const films = [{
    id: 'film-01',
    poster: './img/superman.jpg',
    title: 'Супермен',
    stars: 4,
    price: 1299,
    type: 'Экшн',
    color: 'orange',
    isFavorite: true
}, {
    id: 'film-02',
    poster: './img/lone-runner.jpg',
    title: 'Одинокий странник',
    stars: 3,
    price: 899,
    type: 'Вестерн',
    color: 'brown',
    isFavorite: false
}, {
    id: 'film-03',
    poster: './img/batman.jpg',
    title: 'Бэтмэн',
    stars: 5,
    price: 1499,
    type: 'Экшн',
    color: 'orange',
    isFavorite: false
}];

ReactDOM.render(
    <Films list={films} />,
    document.getElementById('root')
);
//registerServiceWorker();
