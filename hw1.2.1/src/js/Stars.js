import React from 'react';
import Star from "./Star";

function Stars({count = 0}) {
    if ((typeof count === "number") && (count >= 1) && (count <= 5)) {
        let stars = [];
        for (let i = 0; i < count; i++) {
            stars.push(<li key={i}><Star /></li>);
        }
        return (
            <ul className="card-body-stars u-clearfix">
                {stars}
            </ul>
        );
    }
    return null;
}

export default Stars;