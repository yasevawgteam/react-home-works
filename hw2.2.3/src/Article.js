import React, { Component } from 'react';

class Article extends Component {
    constructor(props){
        super(props);
        this.state = {
            isHidden: true
        };
        console.log(props)
    }

    toggleButton() {
        this.setState(
            {isHidden: !this.state.isHidden}
        );
    }

    render() {
        return (
            <section className={this.state.isHidden ? "section" : "section open"}>
                <h3 onClick={this.toggleButton.bind(this)} className="sectionhead">{this.props.data.title}</h3>
                <div className="articlewrap">
                    <div className="article">{this.props.data.text}</div>
                </div>
            </section>
        )
    }
}
export default Article;