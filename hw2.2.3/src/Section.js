import React, { Component } from 'react';

class Section extends Component {
    constructor(props){
        super(props);
        this.state = {
            isHidden: true
        }
    }

    render() {
        return (
            <section className={this.props.className}>
                {this.props.children}
            </section>
        );
    }
}

export default Section;