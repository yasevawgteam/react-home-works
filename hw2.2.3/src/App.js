import React, { Component } from 'react';
import Article from './Article';

class App extends Component {
    constructor(props){
        super(props)
    }
    render() {
        return (
            <div id="accordian">
                <main className="main">
                    <h2 className="title">React</h2>
                    {this.props.data.map((elem, ind) => {
                        return (
                            <Article key={ind} data={elem} />
                        )
                    })}

                </main>
            </div>
        );
    }
}

export default App;
