import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

const filters = ["All", "Websites", "Flayers", "Business Cards"];

const projects = [{
    img: "./i/mon.jpg",
    category: "Business Cards"
}, {
    img: "./i/200.jpg",
    category: "Websites"
}, {
    img: "./i/emi_haze.jpg",
    category: "Websites"
}, {
    img: "./i/codystretch.jpg",
    category: "Websites"
}, {
    img: "./i/Triangle_003.jpg",
    category: "Business Cards"
}, {
    img: "./i/place200x290.png",
    category: "Websites"
}, {
    img: "./i/200.jpg",
    category: "Websites"
}, {
    img: "./i/transmission.jpg",
    category: "Business Cards"
}, {
    img: "./i/place200x290_1.png",
    category: "Websites"
}, {
    img: "./i/place200x290_2.png",
    category: "Flayers"
}, {
    img: "./i/the_ninetys_brand.jpg",
    category: "Websites"
}, {
    img: "./i/dia.jpg",
    category: "Business Cards"
}, {
    img: "./i/Triangle_350x197.jpg",
    category: "Websites"
}, {
    img: "./i/emi_haze.jpg",
    category: "Websites"
}, {
    img: "./i/transmission.jpg",
    category: "Business Cards"
}, {
    img: "./i/Triangle_350x197_1.jpg",
    category: "Websites"
}, {
    img: "./i/place200x290_3.png",
    category: "Flayers"
}];

ReactDOM.render(
    <App filters={filters} projects={projects} />,
    document.getElementById('root')
);

registerServiceWorker();
