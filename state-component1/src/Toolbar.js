import React, { Component } from 'react';

class Toolbar extends React.Component {

    constructor (props) {
        super(props);
    }

    renderFilters (filters)  {
        return filters.map((filter, i) => {
            const className = filter === this.props.selected ? "filter-selected" : "filter";
            return (
                <button
                    className={className}
                    onClick={() => this.props.onSelectFilter(filter)}
                    key={`filter-${i}`} >
                    {filter}
                </button>
            );
        });
    };

    render () {
        return (
            <div className="toolbar">
                {this.renderFilters(this.props.filters)}
            </div>
        );
    }
}


export default Toolbar;