import React, { Component } from 'react';
import Toolbar from './Toolbar';
import Portfolio from './Portfolio';
import './App.css';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            filter: 'All'
        };
    }

    setFilter (filter) {
        this.setState({
            filter: filter
        });
    }

    setProjects () {
        let tempArr = [];
        if (this.state.filter !== 'All') {
            this.props.projects.forEach(project => {
                if (project.category === this.state.filter) tempArr.push(project);
            });
            return tempArr;
        } else {
            return this.props.projects;
        }
    }

    render () {
        return (
            <div>
                <Toolbar
                    filters={this.props.filters}
                    selected={this.state.filter}
                    onSelectFilter={(filter) => this.setFilter(filter)} />
                <Portfolio projects={this.setProjects()} />
            </div>
        );
    }
}

export default App;
