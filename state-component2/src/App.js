import React, { Component } from 'react';
import IconSwitch from './IconSwitch';
import CardsView from './CardView';
import ListView from './ListView';
import ShopCard from './ShopCard';
import ShopItem from './ShopItem';

const VIEW_LIST = "view_list";
const VIEW_MODULE = "view_module";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            icon: VIEW_MODULE,
            cardView: false
        };
    }

    setIcon () {
        this.setState({
            icon: this.state.icon === VIEW_LIST ? VIEW_MODULE : VIEW_LIST,
            cardView: !this.state.cardView
        })
    }

    render() {
        return (
            <div>
                <div className="toolbar">
                    <IconSwitch
                        icon={this.state.icon}
                        onSwitch={this.setIcon.bind(this)} />
                </div>
                {this.renderLayout(this.state.cardView)}
            </div>
        );
    }

    renderLayout(cardView) {
        if (cardView) {
            return (
                <CardsView
                    layout={this.props.layout}
                    cards={this.getShopItems(this.props.products, cardView)} />
            );
        }
        return (<ListView items={this.getShopItems(this.props.products, cardView)} />);
    }

    getShopItems(products, cardView) {
        return products.map(product => {
            let cardProps = {
                title: product.name,
                caption: product.color,
                img: product.img,
                price: `$${product.price}`
            };
            if (cardView) {
                return (
                    <ShopCard {...cardProps}/>
                );
            }
            return (<ShopItem {...cardProps}/>)
        });
    }
}

export default App;
